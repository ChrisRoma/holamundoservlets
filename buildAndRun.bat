@echo off
call mvn clean package
call docker build -t com.mycompany/holamundoServlets .
call docker rm -f holamundoServlets
call docker run -d -p 9080:9080 -p 9443:9443 --name holamundoServlets com.mycompany/holamundoServlets